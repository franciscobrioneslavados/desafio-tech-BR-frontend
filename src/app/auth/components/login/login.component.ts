import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../@core/services/auth/auth.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as rutHelpers from 'rut-helpers';
import {AuthRequestInterface, AuthResponseInterface} from '../../../@core/interfaces/auth.interfaces';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  date = Date.now();
  authForm: FormGroup;
  dniMessageError: null | string;
  dniUser: string;
  isLoading = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private toasterService: ToastrService,
  ) {
    this.createAuthForm();
  }

  ngOnInit(): void {
  }

  private createAuthForm() {
    this.authForm = this.fb.group({
      dni: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  onSubmitForm(): void {
    localStorage.clear();
    this.isLoading = true;
    const authRequest: AuthRequestInterface = this.authForm.getRawValue();
    authRequest.dni = this.dniFormat(authRequest.dni);
    if (authRequest.dni) {
      this.authService.onLogin(authRequest).then(async (response: AuthResponseInterface) => {
          this.isLoading = false;
          await this.router.navigate(['/main/home']);
      }).catch(e => {
        this.toasterService.info(e.error);
      });
    }
  }

   private dniFormat(dni: string): string {
    this.dniMessageError = null;
    if (rutHelpers.rutValidate(dni)) {
      return rutHelpers.rutClean(dni);
    }
    this.dniMessageError = 'RUT invalido';
  }


}
