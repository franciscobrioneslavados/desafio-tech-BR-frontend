import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../@core/services/auth/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import * as rutHelpers from 'rut-helpers';
import {AuthRequestInterface, RegisterRequestInterface} from '../../../@core/interfaces/auth.interfaces';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  date = Date.now();
  authForm: FormGroup;
  dniMessageError: null | string;
  dniUser: string;
  isLoading = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private toasterService: ToastrService,
  ) {
    this.createAuthForm();
  }

  private createAuthForm() {
    this.authForm = this.fb.group({
      dni: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]],
      firstName: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  onSubmitForm() {
    this.isLoading = true;
    const authRequest: RegisterRequestInterface = this.authForm.getRawValue();
    authRequest.dni = this.dniFormat(authRequest.dni);
    this.authService.onRegister(authRequest).then(async response => {
      if (response.code === 201) {
        this.toasterService.success('Register success');
        this.isLoading = false;
        await this.router.navigate(['auth/login']);
      }
    }).catch((e) => {
      this.toasterService.info(e.error.message);
    });
  }

  private dniFormat(dni: string): string {
    this.dniMessageError = null;
    if (rutHelpers.rutValidate(dni)) {
      return rutHelpers.rutClean(dni);
    }
    this.dniMessageError = 'RUT invalido';
  }
}
