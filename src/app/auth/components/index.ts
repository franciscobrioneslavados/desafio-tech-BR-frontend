import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';


export const AUTH_COMPONENTS = [
  LoginComponent,
  RegisterComponent
];

