import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../@core/services/auth/auth.service';
import {UserInterface} from '../../../@core/interfaces/user.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  user: UserInterface;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.user = this.authService.getUserFromToken();
  }

  async onLogout() {
    await this.authService.onSignOut();
  }
}
