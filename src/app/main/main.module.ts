import { NgModule } from '@angular/core';
import {MainRoutingModule} from './main-routing.module';
import {MainComponent} from './main.component';
import {CommonModule} from '@angular/common';
import {MAIN_COMPONENTS} from './components';
import {CoreModule} from '../@core/core.module';
import {ModalModule} from 'ngx-bootstrap/modal';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    MainComponent,
    ...MAIN_COMPONENTS,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    CoreModule,
    ModalModule.forRoot(),
    SharedModule,
  ],
  providers: [],
  bootstrap: [MainComponent]
})
export class MainModule { }
