import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './main.component';
import {HomeComponent} from './components/home/home.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {TransfersComponent} from './components/transfers/transfers.component';
import {ProfileComponent} from './components/profile/profile.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        data: {breadcrumb: 'HOME', icon: 'home'}
      },
      {
        path: 'transfer',
        component: TransfersComponent,
        data: {breadcrumb: 'TRANSFERS', icon: 'exchange-alt'}
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {breadcrumb: 'PROFILE', icon: 'user'}
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: '**',
        component: NotFoundComponent
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
