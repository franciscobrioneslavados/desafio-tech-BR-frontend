import {Component, OnDestroy, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ModalBalanceComponent} from '../../../shared/components/modal-balance/modal-balance.component';
import {AccountService} from '../../../@core/services/account/account.service';
import {AccountInterface, AccountResponseInterface} from '../../../@core/interfaces/account.interface';
import {HistoryService} from '../../../@core/services/history/history.service';
import {HistoryInterface, HistoryInterfaceResponse} from '../../../@core/interfaces/history.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  modalRef: BsModalRef;
  listAccounts = ['cc', 'vista', '10%'];
  account: AccountInterface;
  histories: HistoryInterface[] = [];

  constructor(
    private modalService: BsModalService,
    private accountService: AccountService,
    private historyService: HistoryService
  ) {
  }

  ngOnInit(): void {
    this.data();
  }

  addBalances() {
    this.modalRef = this.modalService.show(ModalBalanceComponent, {
      initialState: {
        title: 'Añadir fondos',
        isRemove: false,
        data: {account: this.account},
      },
      class: 'gray modal-md'
    });
    this.modalService.onHide.subscribe((event) => {
      this.data();
    });
  }

  removeBalances() {
    this.modalRef = this.modalService.show(ModalBalanceComponent, {
      initialState: {
        title: 'Retirar fondos',
        isRemove: true,
        data: {account: this.account},
      },
      class: 'gray modal-md'
    });
    this.modalService.onHide.subscribe((event) => {
      this.data();
    });
  }

  private data() {
    this.accountService.getAccount().then((response: AccountResponseInterface) => {
      this.account = response.account;
    });
    this.historyService.getHistories().then((response: HistoryInterfaceResponse) => {
      this.histories = response.data.filter((data) => !data.transferId);
    });
  }

  difference(a, b) {
    return Math.abs(a - b);
  }

  ngOnDestroy(): void {
    this.account = {};
    this.histories = [];
  }


}
