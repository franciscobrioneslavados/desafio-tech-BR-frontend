import { Component, OnInit } from '@angular/core';
import {AccountInterface, AccountResponseInterface} from '../../../@core/interfaces/account.interface';
import {HistoryInterface, HistoryInterfaceResponse} from '../../../@core/interfaces/history.interface';
import {AccountService} from '../../../@core/services/account/account.service';
import {HistoryService} from '../../../@core/services/history/history.service';
import {ModalBalanceComponent} from '../../../shared/components/modal-balance/modal-balance.component';
import {ModalTransfersComponent} from '../../../shared/components/modal-transfers/modal-transfers.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-transfers',
  templateUrl: './transfers.component.html',
  styleUrls: ['./transfers.component.scss']
})
export class TransfersComponent implements OnInit {
  account: AccountInterface;
  histories: HistoryInterface[] = [];
  modalRef: BsModalRef;

  constructor(
  private modalService: BsModalService,
  private accountService: AccountService,
  private historyService: HistoryService
  ) { }

  ngOnInit(): void {
    this.data();
  }

  transferBalances() {
    this.modalRef = this.modalService.show(ModalTransfersComponent, {
      initialState: {
        title: 'Transferir fondos',
        data: {account: this.account},
      },
      class: 'gray modal-md'
    });
    this.modalService.onHide.subscribe((event) => {
      this.data();
    });
  }

  private data() {
    this.accountService.getAccount().then((response: AccountResponseInterface) => {
      this.account = response.account;
    });
    this.historyService.getHistories('transfer').then((response: HistoryInterfaceResponse) => {
      this.histories = response.data.filter((data) => data.transferId);
    });
  }

  difference(a, b) { return Math.abs(a - b); }

}
