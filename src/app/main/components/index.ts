import {HomeComponent} from './home/home.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {NavbarComponent} from '../layout/navbar/navbar.component';
import {ProfileComponent} from './profile/profile.component';
import {TransfersComponent} from './transfers/transfers.component';


export const MAIN_COMPONENTS = [
  HomeComponent,
  NotFoundComponent,
  NavbarComponent,
  ProfileComponent,
  TransfersComponent
];
