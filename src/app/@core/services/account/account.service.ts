import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import decode from 'jwt-decode';
import {AccountRequestInterface, AccountResponseInterface} from '../../interfaces/account.interface';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private ACCESS_TOKEN = localStorage.getItem('ACCESS_TOKEN');
  private URL_REST = environment.URL_ENDPOINTS;

  constructor(
    private http: HttpClient,
  ) {
  }

  public getAccount(): Promise<AccountResponseInterface> {
    // @ts-ignore
    const userId = decode(this.ACCESS_TOKEN).user.id;
    return this.http.get<AccountResponseInterface>(`${this.URL_REST}/api/accounts/${userId}`).toPromise();
  }

  public getAccountByDni(dni: string) {
    return this.http.get<AccountResponseInterface>(`${this.URL_REST}/api/accounts/user/${dni}`).toPromise();
  }

  public putAccount(accountRequest: { balance: number; id: any; typeOfAccount: any; userId: any, typeOperation: string }): Promise<{ message, code }> {
    return this.http.put<{ message, code }>(`${this.URL_REST}/api/accounts/${accountRequest.userId}`, accountRequest, {}).toPromise();
  }

  public putTransfer(accountRequest: AccountRequestInterface, accountTargetRequest: AccountRequestInterface): Promise<{ message, code }> {
    return this.http.patch<{ message, code }>(`${this.URL_REST}/api/accounts/transfers`, {
      accountRequest,
      accountTargetRequest
    }, {}).toPromise();
  }
}
