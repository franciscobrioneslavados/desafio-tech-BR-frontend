import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import decode from 'jwt-decode';
import {AuthRequestInterface, AuthResponseInterface, RegisterRequestInterface} from '../../interfaces/auth.interfaces';
import {UserInterface} from '../../interfaces/user.interface';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private ACCESS_TOKEN = localStorage.getItem('ACCESS_TOKEN');
  private URL_REST = environment.URL_ENDPOINTS;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
    private router: Router
  ) {
  }

  public onLogin(user: AuthRequestInterface): Promise<AuthResponseInterface> {
    return new Promise((resolve, reject) => {
      this.http
        .post<AuthResponseInterface>(`${this.URL_REST}/api/users/login`, user, {})
        .toPromise()
        .then((response: AuthResponseInterface) => {
          localStorage.setItem('ACCESS_TOKEN', response.access_token);
          localStorage.setItem('EXPIRES', response.expires);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public onRegister(authRequest: RegisterRequestInterface): Promise<AuthResponseInterface> {
    return this.http.post<AuthResponseInterface>(`${this.URL_REST}/api/users/register`, authRequest, {}).toPromise();
  }


  public getUserFromToken(): UserInterface {
    // @ts-ignore
    return decode(localStorage.getItem('ACCESS_TOKEN')).user;
  }

  public isAuthenticated(): boolean {
    return !this.jwtHelper.isTokenExpired(this.ACCESS_TOKEN);
  }

  public async onSignOut() {
    localStorage.clear();
    await this.router.navigate(['/auth']);
  }
}
