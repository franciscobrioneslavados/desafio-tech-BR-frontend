import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import decode from 'jwt-decode';
import {HistoryInterfaceResponse} from '../../interfaces/history.interface';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  private ACCESS_TOKEN = localStorage.getItem('ACCESS_TOKEN');
  private URL_REST = environment.URL_ENDPOINTS;

  constructor(private http: HttpClient) { }

  public getHistories(typeOperation?: string): Promise<HistoryInterfaceResponse>{
    // @ts-ignore
    const userId = decode(this.ACCESS_TOKEN).user.id;
    let url;
    if (typeOperation) {
      url = `${this.URL_REST}/api/history/${userId}?typeOperation=${typeOperation}`;
    }
    url = `${this.URL_REST}/api/history/${userId}`;

    return this.http.get<HistoryInterfaceResponse>(url).toPromise();
  }


}
