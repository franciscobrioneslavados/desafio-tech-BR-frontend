import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExternalService {

  constructor(
    private http: HttpClient,
  ) { }

  public getListBank() {
    return this.http.get('https://raw.githubusercontent.com/ctala/InstitucionesFinancierasChile/master/json/InstitucionesFinancieras.json', {}).toPromise();
  }
}
