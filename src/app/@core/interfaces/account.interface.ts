import {UserInterface} from './user.interface';

export interface AccountResponseInterface {
  account?: AccountInterface;
  code?: number;
  message?: string;
}

export interface AccountInterface {
  balance?: number;
  id?: string;
  typeOfAccount?: string;
  user?: UserInterface;
  userId?: string;
}

export interface AccountRequestInterface {
  balance?: number;
  id?: string;
  typeOfAccount?: string;
  userId?: string;
  typeOperation?: string | any;
}
