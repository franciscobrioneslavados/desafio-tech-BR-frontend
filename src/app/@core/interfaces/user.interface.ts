export interface UserInterface {
  id?: string;
  email?: string;
  password?: string;
  firstname?: string;
  lastname?: string;
  dni?: string;
  reset_password_token?: string;
  reset_password_expires?: string;
  email_confirm?: boolean;
  first_auth?: boolean;
  canLogin?: boolean;
  photoUrl?: string;
  provider_id?: string;
  provider?: string;
  access_token?: string;
  refresh_token?: string;
  createdAt?: Date | string;
  updatedAt?: Date | string;
}

export interface UserResponseInterface {
  message?: string;
  access_token?: string;
  expires?: number;
  user?: UserInterface;
  code?: number;
}
