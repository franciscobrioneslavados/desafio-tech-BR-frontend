import {UserInterface} from './user.interface';

export interface HistoryInterfaceResponse {
  code: number;
  data: HistoryInterface[];
  message: string;
}

export interface HistoryInterface {
  amountAfter: number;
  amountBefore: number;
  id: string;
  transferId?: string | null;
  typeOfOperation: string;
  user: UserInterface;
  userId: string;
  createdAt: Date;
  updatedAt: Date;
}
