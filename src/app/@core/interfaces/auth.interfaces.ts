import {UserInterface} from './user.interface';


export interface AuthRequestInterface {
  dni: string;
  email: string;
  password: string;
}

export interface RegisterRequestInterface {
  dni: string;
  email: string;
  password: string;
  firstName: string;
}

export interface AuthResponseInterface {
  message: string;
  access_token: string;
  expires: string;
  user: UserInterface;
  code: number;
}
