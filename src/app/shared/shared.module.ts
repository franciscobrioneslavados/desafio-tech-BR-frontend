import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DniDirective } from './directives/dni/dni.directive';
import { ModalBalanceComponent } from './components/modal-balance/modal-balance.component';
import { CurrencyPipe } from './pipes/currency/currency.pipe';
import {FormsModule} from '@angular/forms';
import { ModalTransfersComponent } from './components/modal-transfers/modal-transfers.component';
import { BalancePipe } from './pipes/balance/balance.pipe';

@NgModule({
  declarations: [DniDirective, ModalBalanceComponent, CurrencyPipe, ModalTransfersComponent, BalancePipe],
  exports: [
    DniDirective,
    ModalBalanceComponent,
    ModalTransfersComponent,
    BalancePipe
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class SharedModule { }
