import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {AccountService} from '../../../@core/services/account/account.service';
import {ToastrService} from 'ngx-toastr';
import {AccountInterface, AccountRequestInterface} from '../../../@core/interfaces/account.interface';
import * as rutHelpers from 'rut-helpers';

@Component({
  selector: 'app-modal-transfers',
  templateUrl: './modal-transfers.component.html',
  styleUrls: ['./modal-transfers.component.scss']
})
export class ModalTransfersComponent implements OnInit {

  @Input() title: string;
  @Input() data: any;

  balanceAfter: number;
  isLoading: boolean;
  currentBalance = 0;
  account: AccountInterface;
  dniUser: any;

  constructor(
    public modalRef: BsModalRef,
    private accountService: AccountService,
    private toasterService: ToastrService,
    private modalService: BsModalService,
  ) {
  }

  ngOnInit(): void {
    this.account = this.data.account;
  }

  async onTransferBalance() {
    const dniUser = this.dniFormat(this.dniUser);
    const {balance, id, typeOfAccount, userId} = this.data.account;
    if (!this.account.user.dni.includes(this.dniUser)) {
      const {account} = await this.accountService.getAccountByDni(dniUser);
      if (account) {
        if (this.balanceAfter === 0) {
          this.isLoading = false;
          return;
        }
        if (this.balanceAfter > balance) {
          this.toasterService.info('EL saldo a retirar no puede ser mayor a tu saldo actual');
          this.isLoading = false;
          return;
        }
        const accountRequest: AccountRequestInterface = {
          balance: balance - this.balanceAfter,
          id,
          typeOfAccount,
          userId,
          typeOperation: 'balance-down'
        };
        const accountTargetRequest: AccountRequestInterface = {
          balance: account.balance + this.balanceAfter,
          id: account.id,
          typeOfAccount: account.typeOfAccount,
          userId: account.userId,
          typeOperation: 'balance-up'
        };
        this.accountService.putTransfer(accountRequest, accountTargetRequest)
          .then((response)  => {
          if (response.code === 200) {
            this.isLoading = false;
            this.toasterService.success('Transferencia Realizada con exito!');
            this.modalService.setDismissReason(JSON.stringify({done: true}));
            this.modalRef.hide();
          }
        }).catch(e => {
          console.error(e);
        });
      }
    }
  }


  private dniFormat(dni: string): string {
    if (rutHelpers.rutValidate(dni)) {
      return rutHelpers.rutClean(dni);
    }
  }
}


