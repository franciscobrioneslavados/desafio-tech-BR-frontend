import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTransfersComponent } from './modal-transfers.component';

describe('ModalTransfersComponent', () => {
  let component: ModalTransfersComponent;
  let fixture: ComponentFixture<ModalTransfersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTransfersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTransfersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
