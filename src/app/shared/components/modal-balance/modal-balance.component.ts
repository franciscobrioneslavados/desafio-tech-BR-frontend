import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {AccountInterface} from '../../../@core/interfaces/account.interface';
import {AccountService} from '../../../@core/services/account/account.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-modal-balance',
  templateUrl: './modal-balance.component.html',
  styleUrls: ['./modal-balance.component.scss']
})
export class ModalBalanceComponent implements OnInit {

  @Input() title: string;
  @Input() isRemove: boolean;
  @Input() data: any;

  account: AccountInterface;
  balanceAfter: number;
  isLoading: boolean;
  currentBalance = 0;

  constructor(
    public modalRef: BsModalRef,
    private accountService: AccountService,
    private toasterService: ToastrService,
    private modalService: BsModalService,
  ) {

  }

  ngOnInit(): void {
    this.account = this.data.account;
  }

  onBalanceUp() {
    this.isLoading = true;
    const {balance, id, typeOfAccount, userId} = this.data.account;
    if (this.balanceAfter === 0) {
      this.isLoading = false;
      return;
    }
    const accountRequest = {
      balance: balance + this.balanceAfter,
      id,
      typeOfAccount,
      userId,
      typeOperation: 'balance-up'
    };
    this.accountService.putAccount(accountRequest).then((response) => {
      if (response.code === 200) {
        this.isLoading = false;
        this.toasterService.success('Monto añadido a tu cuenta');
        this.modalService.setDismissReason(JSON.stringify({done: true}));
        this.modalRef.hide();
      }
    }).catch(e => {
      console.error(e);
    });
  }

  onBalanceRemove() {
    this.isLoading = true;
    const {balance, id, typeOfAccount, userId} = this.data.account;
    if (this.balanceAfter === 0) {
      this.isLoading = false;
      return;
    }
    if (this.balanceAfter > balance) {
      this.toasterService.info('EL saldo a retirar no puede ser mayor a tu saldo actual');
      this.isLoading = false;
      return;
    }
    const accountRequest = {
      balance: balance - this.balanceAfter,
      id,
      typeOfAccount,
      userId,
      typeOperation: 'balance-down'
    };
    this.accountService.putAccount(accountRequest).then((response) => {
      if (response.code === 200) {
        this.isLoading = false;
        this.toasterService.success('Monto añadido a tu cuenta');
        this.modalService.setDismissReason(JSON.stringify({done: true}));
        this.modalRef.hide();
      }
    }).catch(e => {
      console.error(e);
    });
  }
}
