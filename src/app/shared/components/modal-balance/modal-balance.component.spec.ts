import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBalanceComponent } from './modal-balance.component';

describe('ModalBalanceComponent', () => {
  let component: ModalBalanceComponent;
  let fixture: ComponentFixture<ModalBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
