import { Pipe, PipeTransform } from '@angular/core';
import {CountryCode} from '../../enums/currency.enum';

@Pipe({
  name: 'currency'
})
export class CurrencyPipe implements PipeTransform {
  transform(value: any, currencyCode = 'CL'): string {
    return numberToCurrency(value, currencyCode);
  }
}

export function numberToCurrency(value: any, currencyCode = 'CL'): string {
  if (value != null && !isNaN(value)) {
    if (currencyCode === CountryCode.CL) {
      return CurrencyPipe[currencyCode] + Intl.NumberFormat('de-DE').format(value).toString().split(',')[0];
    }
  }
  return '';
}
