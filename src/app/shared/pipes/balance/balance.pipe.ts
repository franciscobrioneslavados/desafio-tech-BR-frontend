import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'balance'
})
export class BalancePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    switch (value) {
      case 'balance-up': return 'Carga de Fondos';
      case 'balance-down': return 'Retiro de Fondos';
      case 'balance-transfer': return 'Transferencia de Fondos';
    }
    return null;
  }

}
