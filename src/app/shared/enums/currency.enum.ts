export enum CountryCode {
  CL = 'CL'
}

export enum Currency {
  CL = '$'
}
